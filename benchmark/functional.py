#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys

NOT_FOUND = object()
MAX_HITS = sys.maxsize
KEY, VALUE, HITS, PREV, NEXT, THRESHOLD, FLAG, CACHE, MAXSIZE = range(9)
ITEMSIZE = THRESHOLD
ROOTSIZE = 9


def lfudacache(maxsize, _max_hits=MAX_HITS, _prev=PREV, _threshold=THRESHOLD):
    '''
    Less Frequenty Used with Dynamic Aging cache.

    Implementation note:
        Instances will be created from their own different subclass.

        As a side effect, the effective instance type won't be this class, so
        avoid :func:`type` conparisons and use :func:`isinstance` instead.

    How it works:
        * Every cache hit increases item HIT counter.
        * Every cache miss increases THRESHOLD, until max HITS is reached.
        * When full, a new cache item will only be accepted if THRESHOLD is
          above or equals the less frequently used item's HIT counter. Said
          item is evicted.
        * When a new item is cached, its HIT counter is set equal to THRESHOLD
          itself.
        * When an existing item is updated, its HIT counter is incremented 
          by 1 above THRESHOLD.
        * When any item's HITS reaches MAX_HITS, said value is substracted
          from every HIT and THRESHOLD counter.
    '''
    root = [None, None, _max_hits, None, None, 0, False, {}, maxsize]
    root[_prev:_threshold] = root, root
    return root


def lfudaget(root, key, default=None, _cache=CACHE, _not_found=NOT_FOUND,
             _threshold=THRESHOLD, _prev=PREV, _hits=HITS, _next=NEXT,
             _max_hits=MAX_HITS, _value=VALUE, _itemsize=ITEMSIZE, _flag=FLAG):
    '''
    LFUDACache.get(key[,default]) -> value

    Get value of key from cache.

    :param key: cache item key
    :param default: optional default parameter
    :returns: cache item value
    '''
    item = root[_cache].get(key, _not_found)
    if item is _not_found:
        if root[_threshold] < root[_prev][_hits]:
            root[_threshold] += 1
        return default

    item[_hits] += 1

    # already sorted
    if item[_hits] < item[_next][_hits]:
        return item[_value]

    # avoid overflow
    if item[_hits] == _max_hits:
        for item in root[_cache].values():
            item[_hits] -= _max_hits
        root[_threshold] = 0

    # extract
    item[_prev][_next] = item[_next]
    item[_next][_prev] = item[_prev]

    # position (start on next)
    next = item[_next][_next]
    hits = item[_hits]
    while hits >= next[_hits]:
        next = next[_next]

    # insert
    item[_prev:_itemsize] = next[_prev], next
    item[_prev][_next] = item
    item[_next][_prev] = item

    # update sort count
    root[_flag] = False

    return item[_value]


def lfudaset(root, key, value, _cache=CACHE, _not_found=NOT_FOUND, _next=NEXT,
             _threshold=THRESHOLD, _hits=HITS, _key=KEY, _prev=PREV,
             _value=VALUE, _max_hits=MAX_HITS, _itemsize=ITEMSIZE, _flag=FLAG,
             _maxsize=MAXSIZE):
    '''
    LFUDACache[key] = value

    Set value for key in cache.

    :param key: cache item key
    :param value: cache item value
    '''
    item = root[_cache].get(key, _not_found)

    if item is _not_found:
        if len(root[_cache]) == root[_maxsize]:
            item = root[_next]
            if root[_threshold] < item[_hits]:
                return value  # not expirable yet

            # uncache
            del root[_cache][item[_key]]

            # extract
            item[_prev][_next] = item[_next]
            item[_next][_prev] = item[_prev]

        item = [key, value, root[_threshold], root, root[_next]]
        root[_cache][key] = item
    else:
        item[_value] = value
        item[_hits] = max(root[_threshold], item[_hits]) + 1

        # already sorted
        if item[_hits] < item[_next][_hits]:
            return value

        # avoid overflow
        if item[_hits] == _max_hits:
            for item in root[_cache].values():
                item[_hits] -= _max_hits
            root[_threshold] = 0

        # extract
        item[_prev][_next] = item[_next]
        item[_next][_prev] = item[_prev]

    # position
    next = item[_next]
    hits = item[_hits]
    while hits >= next[_hits]:
        next = next[_next]

    # insert
    item[_prev:_itemsize] = next[_prev], next
    item[_prev][_next] = item
    item[_next][_prev] = item

    # update sort count
    root[_flag] = False

    return value


def lfudadel(root, key, _cache=CACHE, _not_found=NOT_FOUND, _prev=PREV,
             _next=NEXT):
    '''
    del LFUDACache[key]

    Remove specified key and return the corresponding value.
    If key is not found KeyError is raised.

    :param key: cache item key
    :param value: cache item value
    '''
    item = root[_cache].pop(key, _not_found)
    if item is _not_found:
        raise KeyError('Key %r not found.' % key)

    # extract
    item[_prev][_next] = item[_next]
    item[_next][_prev] = item[_prev]


def lfudahas(root, key, _cache=CACHE):
    '''
    key in LFUDACache -> True if cached, False otherwise

    :returns: True if key is cached, False otherwise
    '''
    return key in root[_cache]


def lfudapop(root, key, default=None, _cache=CACHE, _not_found=NOT_FOUND,
             _next=NEXT, _prev=PREV, _value=VALUE):
    '''
    LFUDACache.pop(key[,default]) -> value

    Remove specified key and return the corresponding value.
    If key is not found, default is returned if given, otherwise
    KeyError is raised.

    :param key: cache item key
    :param default: optional default parameter
    :returns: cache item value
    '''
    item = root[_cache].pop(key, _not_found)
    if item is _not_found:
        if default is _not_found:
            raise KeyError('Key %r not found.' % key)
        return default

    # extract
    item[_prev][_next] = item[_next]
    item[_next][_prev] = item[_prev]

    return item[_value]


def lfudaitems(root, _flag=FLAG, _prev=PREV, _key=KEY, _value=VALUE):
    '''
    LFUDACache.items() => iterable of key and value tuples

    Iterable of cache pairs (key, value) sorted from most to lesser
    frequently used.

    :yields: (key, value) tuples
    '''
    root[_flag] = True
    prev = root[_prev]
    while prev is not root and root[_flag]:
        yield prev[_key], prev[_value]
        prev = prev[_prev]

    if not root[_flag]:
        raise RuntimeError('cache order changed during iteration')


def lfudakeys(root):
    '''
    iter(LFUDACache) -> iterable of keys
    LFUDACache.keys() -> iterable of keys

    Iterable of cache keys sorted from most to lesser
    frequently used.

    :yields: key
    '''
    for k, _ in lfudaitems(root):
        yield k


def lfudavalues(root):
    '''
    LFUDACache.values() -> iterable of values

    Iterable of cache values sorted from most to lesser
    frequently used.

    :yields: value
    '''
    for _, v in lfudaitems(root):
        yield v


def lfudaclear(root, _cache=CACHE, _prev=PREV, _rootsize=ROOTSIZE):
    '''
    LFUDA.clear()

    Evict the entire cache.
    '''
    root[_cache].clear()
    root[_prev:_rootsize] = root, root, 0, 0


def lfudathreshold(root, _threshold=THRESHOLD):
    '''
    Current threshold level

    :returns: current threshold level
    :rtype: int
    '''
    return root[_threshold]


def lfudasize(root, _cache=CACHE):
    '''
    len(LFUDACache) -> current cache size
    '''
    return len(root[_cache])
