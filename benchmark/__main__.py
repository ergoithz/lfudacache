import sys
import pprint
import timeit

from . import implementations


def main(argv=()):
    n = 10000
    f = '{:.7f}'.format
    m = '{0}:{1.__module__}.{1.__name__}'.format
    t = timeit.timeit
    pprint.pprint({
        name: f(t(lambda: tester(root, get, set, pop), number=n))
        for tag, props in implementations.items()
        for name, root, tester in (
            (m(tag, props[0]), props[0](10), props[1]),
            )
        for get, set, pop in ((
            (
                root.get,
                root.setitem if hasattr(root, 'setitem') else root.__setitem__,
                root.pop,
            )
            if len(props) == 2 else
            props[2:5]
            ),)
        })


if __name__ == '__main__':
    main(sys.argv)
