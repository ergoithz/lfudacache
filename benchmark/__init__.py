# -*- coding: UTF-8 -*-

import sys
import os
import os.path

from . import functional as func
from . import v1

sys.path.append(os.path.abspath('..'))
import lfudacache as v2  # noqa


class Dummy(dict):
    def __init__(self, maxsize):
        self.maxsize = maxsize


def objtester(cache, getitem, setitem, popitem):
    setitem('a', 1)
    setitem('b', 1)
    setitem('c', 1)
    setitem('d', 1)
    setitem('e', 1)
    setitem('f', 1)
    setitem('g', 1)
    setitem('h', 1)
    setitem('i', 1)
    setitem('j', 1)
    setitem('k', 1)
    setitem('l', 1)
    setitem('m', 1)
    setitem('n', 1)
    popitem('m')
    getitem('a')
    getitem('k')
    getitem('k')
    getitem('k')
    getitem('k')
    getitem('k')
    getitem('k')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('f')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    getitem('a')
    setitem('a', 1)


def functester(cache, getitem, setitem, popitem):
    setitem(cache, 'a', 1)
    setitem(cache, 'b', 1)
    setitem(cache, 'c', 1)
    setitem(cache, 'd', 1)
    setitem(cache, 'e', 1)
    setitem(cache, 'f', 1)
    setitem(cache, 'g', 1)
    setitem(cache, 'h', 1)
    setitem(cache, 'i', 1)
    setitem(cache, 'j', 1)
    setitem(cache, 'k', 1)
    setitem(cache, 'l', 1)
    setitem(cache, 'm', 1)
    setitem(cache, 'n', 1)
    popitem(cache, 'm')
    getitem(cache, 'a')
    getitem(cache, 'k')
    getitem(cache, 'k')
    getitem(cache, 'k')
    getitem(cache, 'k')
    getitem(cache, 'k')
    getitem(cache, 'k')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'f')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    getitem(cache, 'a')
    setitem(cache, 'a', 1)


implementations = {
    'dummy': (Dummy, objtester),
    'v2': (v2.LFUDACache, objtester),
    'v1': (v1.LFUDACache, objtester),
    'v0': (
        func.lfudacache,
        functester,
        func.lfudaget,
        func.lfudaset,
        func.lfudapop,
        ),
    }
