# -*- coding: UTF-8 -*-

app = 'lfudacache'
description = 'Less Frequently Used with Dynamic Aging'
version = '0.0.2'
license = 'MIT'
author_name = 'Felipe A. Hernandez'
author_mail = 'ergoithz@gmail.com'
author = '%s <%s>' % (author_name, author_mail)
url = 'https://gitlab.com/ergoithz/lfudacache'
